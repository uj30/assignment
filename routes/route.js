const express = require('express');
const router = express.Router();
const customerService = require('../services/customerService');

//Inserting Customer Data
router.get('/insertCustomerData', customerService.insertCustomerData);

//Filter Data According To Location
router.get('/filterCustomerData', customerService.filterCustomerData);

module.exports = router;
