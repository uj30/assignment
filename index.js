const express = require('express');
const mongoose = require('mongoose');
const app = express();
const route = require('./routes/route');
const port = 3000;
const databaseName = 'mongodb://localhost:27017/Customers'

//For Connecting Database
mongoose.connect(databaseName, { useNewUrlParser: true, useCreateIndex: true });
mongoose.connection.on('connected', function () {
    console.log('Connected to database');
});
mongoose.connection.on('error', function (err) {
    if (err) {
        console.log('Connection Error:', err);
    }
});

app.use('/api', route);

app.listen(port);
console.log('Running on port 3000');