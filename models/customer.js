const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let CustomerSchema = new Schema({
    name: {                  //For Name
        type: String,
        required: true
    },
    user_id: {               //For UserId
        type: Number,
        required: true,
        unique: true,
    },
    location: {             //For GeoJson Point 
        type: {
            type: String,
            enum: ['Point'],
            required: true
        },
        coordinates: {
            type: [Number],
            required: true
        }
    }
}
);
CustomerSchema.index({ location: '2dsphere' });   //To support queries like calculate coordinates
module.exports = mongoose.model('Customer', CustomerSchema);