const Customer = require('../models/customer');
var fs = require("fs");

var insertCustomerData = function (req, res, next) {

    //Removing all documents from collection
    Customer.remove((err) => {
        console.log('Error In Deleting')
    })

    try {
        //Reading CustomerData File
        fs.readFile("CustomersData.txt", "utf-8", async (err, data) => {
            if (err) {
                console.log('Error Reading File:', err)
            }
            let info = data.split('\n')
            let customerInfo = info.map(JSON.parse)
            for (let customer of customerInfo) {
                let customer_data = {
                    "user_id": customer.user_id,
                    "name": customer.name,
                    "location": {
                        "type": "Point",
                        "coordinates": [+customer.longitude, +customer.latitude]
                    }
                }

                await insertData(customer_data)
            }
        })
    }
    catch (err) {
        console.log('Error')
    }
}

//Customer Data Insert In Database
async function insertData(customer_data) {
    let customer_detail = await new Customer(customer_data)
    customer_detail.save()
}

filterCustomerData = async function (req, res, next) {
    var pipeline = [{
        $geoNear: {
            near: { type: "Point", coordinates: [-6.257664, 53.339428] },  //Coordinates of Dublin
            maxDistance: 100000,    //Default in meter so multiply by 1000 to convert to kilometer
            distanceMultiplier: 0.001,   //Use GeoJSON point 
            spherical: true,   //Calculate distance using spherical geometry as of Earth
            distanceField: "dist.calculated"  //Contains Calculated Distance
        }
    },
    {
        $project: { user_id: 1, name: 1, _id: 0 }    //Showing only user_id and name
    },
    {
        $sort: { user_id: 1 }       //Ascending Order Of user_id
    }
    ]

    //Aggregate Geonear Query On Customer Data
    let response = await Customer.aggregate(pipeline)
    res.json(response);
}

module.exports = {
    insertCustomerData,
    filterCustomerData
}